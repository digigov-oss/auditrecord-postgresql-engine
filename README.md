# AuditRecordPostgreSqlEngine

Complementary engine for [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB) module to use famous PostgreSQL DB for storage.


### Prostgresql instalation
To work with Postgresql, you need to use `PostgresqlEngine` instead of `FileEngine`
Moreover, have to install the native `libpg` library for Postgresql.

- On macOS: brew install libpq
- On Ubuntu/Debian: apt-get install libpq-dev g++ make
- On RHEL/CentOS: yum install postgresql-devel


To make it work,  pass environment variables according to the documentation of the LIBPGSQL library. https://www.postgresql.org/docs/9.1/libpq-envars.html

Please remember that protocol and/or transaction id sequences will be created in the database if not provided.

You can set the reset protocol number type at "daily","monthly" or "yearly". By default, it is "innumerable", i.e. never resets.

If you select one of the reset types, a new sequence will be created in the database for protocol needs as protxxxxx_seq.
So if you do not need to keep tracking on application, you may need to clean up those sequences from time to time using a cron job.

#### Examples
```
//you can use the PostgresSqlEngine via enviroment variables
import auditRecordDB from '@digigov-oss/gsis-audit-record-db';
import {PostgreSqlEngine} from '@digigov-oss/auditrecord-postgresql-engine';
const main = () =>{
process.env.PGHOST='localhost'
process.env.PGUSER='dbuser'
process.env.PGPASSWORD='secretpassword'
process.env.PGDATABASE='audit'
process.env.PGPORT='5432'

console.log(auditRecordDB({},new PostgresSqlEngine()))
}

```

```
//or via connection string
import auditRecordDB from '@digigov-oss/gsis-audit-record-db';
import {PostgreSqlEngine} from '@digigov-oss/auditrecord-postgresql-engine';
const connectionString = 'postgresql://dbuser:secretpassword@localhost:5432/audit'
const main = () =>{
console.log(auditRecordDB({},new PostgresSqlEngine(connectionString)))
}
```

```
//The postgresql engine asumes that you have already create the table `audit_records` in the database, and that the table has the following columns:
//auditUnit: text
//auditTransactionId: text
//auditProtocol: text
//auditTransactionDate: text
//auditUserIp: text
//auditUserId: text
//
//if you have already a table on Postgresql you can use it by mapping the columns to the AuditRecord
import auditRecordDB from '@digigov-oss/gsis-audit-record-db';
import {PostgreSqlEngine} from '@digigov-oss/auditrecord-postgresql-engine';
const connectionString = 'postgresql://dbuser:secretpassword@localhost:5432/audit'
const main = () =>{
console.log(auditRecordDB({},new PostgresSqlEngine(connectionString,{
tableName:'audit_records',
columns:{
  auditUnit:'audit_unit',
  auditTransactionId:'audit_transaction_id',
  auditProtocol:'audit_protocol',
  auditTransactionDate:'audit_transaction_date',
  auditUserIp:'audit_user_ip',
  auditUserId:'audit_user_id'
  }
})))
}
```


