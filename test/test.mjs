import generateAuditRecord from '@digigov-oss/gsis-audit-record-db'
import Client from 'pg-native';
import {PostgreSqlEngine} from '../dist/esm/index.js';

const test = async () => {
    const ar = await generateAuditRecord({}, new PostgreSqlEngine('postgres://postgres:postgres@localhost:5432/postgres'));
    console.log(ar);
}

const test1 = async () => {
    const databaseSettings = {
        tableName: 'temp_log',
        columns: {
            auditUnit: 'audit_Unit',
            auditTransactionId: 'audit_Transaction_Id',
            auditProtocol: 'audit_Protocol',
            auditTransactionDate: 'audit_Transaction_Date',
            auditUserIp: 'audit_User_Ip',
            auditUserId: 'audit_User_Id'
        }
    }
    const dbs=JSON.parse(JSON.stringify(databaseSettings));
    const connection='postgres://postgres:postgres@localhost:5432/postgres'
    const client = new Client(connection);
    client.connectSync();
    client.querySync("CREATE TABLE IF NOT EXISTS "+dbs.tableName+" ("+dbs.columns.auditUnit+" varchar(255), "+dbs.columns.auditTransactionId+" varchar(255), "+dbs.columns.auditProtocol+" varchar(255), "+dbs.columns.auditTransactionDate+" varchar(20), "+dbs.columns.auditUserIp+" varchar(16), "+dbs.columns.auditUserId+" varchar(255));");
    const ar = await generateAuditRecord({}, new PostgreSqlEngine(connection,databaseSettings));
    console.log(ar);
    client.querySync("DROP TABLE "+dbs.tableName);
}


console.log("Audit Record Generator, simple test on postgresql");
test();
console.log("Audit Record Generator, custom table and columns on postgresql");
test1();

