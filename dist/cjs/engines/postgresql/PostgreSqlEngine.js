"use strict";
//To check this engine you have to have a PostgreSQL database installed and running.
//The database must be configured to allow connections from the host that the application is running on.
//The database must also have the following table:
//table audit_records
//  auditUnit: varchar(255);
//  auditTransactionId: varchar(255);
//  auditProtocol: varchar(255);
//  auditTransactionDate:  varchar(20);
//  auditUserIp: varchar(16);
//  auditUserId: varchar(255);
// if you do not provide the table name, the engine will create it for you.
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _PostgreSqlEngine_table, _PostgreSqlEngine_columnNames, _PostgreSqlEngine_client;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgreSqlEngine = void 0;
//You can use Docker to run PostgreSQL for your tests.
//`docker run -d --name audit-postgres -e POSTGRES_PASSWORD=audit --network host postgres`
//`docker exec -it audit-postgres psql -U postgres -h localhost -c "CREATE DATABASE audit;"`
//`docker exec -it audit-postgres psql -U postgres -h localhost -c "CREATE TABLE audit_log (auditUnit varchar(255), auditTransactionId varchar(255), auditProtocol varchar(255), auditTransactionDate varchar(20), auditUserIp varchar(16), auditUserId varchar(255));"`
//You can also map the fields of already existent table to the fields in the audit log.
const pg_native_1 = __importDefault(require("pg-native"));
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class PostgreSqlEngine
 * @implements AuditEngine
 * @param {string} connectionString - connect to db via connection string
 * @param {DatabaseSettings} dbSettings - database settings
 */
class PostgreSqlEngine {
    /**
     * @description constructor
     * @param {string} connectionString - connect to db via connection string
     * @param {DatabaseSettings} settings - settings for the database
     * @param {AuditRecord} columnNames - column names
     * @memberof PostgreSqlEngine
     */
    constructor(connectionString = "", dbSettings = {}) {
        _PostgreSqlEngine_table.set(this, void 0);
        _PostgreSqlEngine_columnNames.set(this, void 0);
        _PostgreSqlEngine_client.set(this, void 0);
        const connection = connectionString != "" ? { connectionString } : {};
        __classPrivateFieldSet(this, _PostgreSqlEngine_table, dbSettings.tableName || "audit_records", "f");
        __classPrivateFieldSet(this, _PostgreSqlEngine_columnNames, dbSettings.columns || {
            auditUnit: "auditUnit",
            auditTransactionId: "auditTransactionId",
            auditProtocol: "auditProtocol",
            auditTransactionDate: "auditTransactionDate",
            auditUserIp: "auditUserIp",
            auditUserId: "auditUserId"
        }, "f");
        __classPrivateFieldSet(this, _PostgreSqlEngine_client, new pg_native_1.default(connection), "f");
        __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").connectSync();
        if (!dbSettings.tableName) {
            __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("CREATE TABLE IF NOT EXISTS " + __classPrivateFieldGet(this, _PostgreSqlEngine_table, "f") + " (" + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUnit + " varchar(255), " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditTransactionId + " varchar(255), " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditProtocol + " varchar(255), " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditTransactionDate + " varchar(20), " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUserIp + " varchar(16), " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUserId + " varchar(255));");
        }
    }
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record) {
        const data = JSON.stringify(record, null, 2);
        try {
            __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("INSERT INTO " + __classPrivateFieldGet(this, _PostgreSqlEngine_table, "f") + " (" + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUnit + "," + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditTransactionId + "," + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditProtocol + "," + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditTransactionDate + "," + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUserIp + "," + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditUserId + ") \
                        VALUES ('" + record.auditUnit + "','" + record.auditTransactionId + "','" + record.auditProtocol + "','" + record.auditTransactionDate + "','" + record.auditUserIp + "','" + record.auditUserId + "');");
            return record;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Get a record from the database
     * @param auditTransactionId: string - transaction id
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId) {
        try {
            let data = {};
            const res = __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("SELECT * FROM " + __classPrivateFieldGet(this, _PostgreSqlEngine_table, "f") + " WHERE " + __classPrivateFieldGet(this, _PostgreSqlEngine_columnNames, "f").auditTransactionId + "='" + auditTransactionId + "';");
            data = res[0];
            return data;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof FileEngine
     * @method seq
     */
    seq() {
        try {
            __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("CREATE SEQUENCE IF NOT EXISTS " + __classPrivateFieldGet(this, _PostgreSqlEngine_table, "f") + "_seq START 1");
            const res = __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("SELECT nextval('" + __classPrivateFieldGet(this, _PostgreSqlEngine_table, "f") + "_seq');");
            return res[0].nextval;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @description Generate a new protocol number
     * @param path
     * @returns string
     * @memberof FileEngine
     * @method protocol
     */
    pn(pnreset) {
        try {
            let protocol_split = "aion";
            let protocol_date = new Date().toISOString().split('T')[0];
            switch (pnreset) {
                case "daily":
                    protocol_split = protocol_date;
                    break;
                case "monthly":
                    protocol_split = protocol_date.split('-')[0] + "-" + protocol_date.split('-')[1];
                    break;
                case "yearly":
                    protocol_split = protocol_date.split('-')[0];
                    break;
                case "innumerable":
                    protocol_split = "aion";
                    break;
            }
            const seqName = "prot" + protocol_split.replace(/-/g, '');
            //create sequence for protocol_split if not exists
            __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("CREATE SEQUENCE IF NOT EXISTS " + seqName + "_seq START 1");
            const res = __classPrivateFieldGet(this, _PostgreSqlEngine_client, "f").querySync("SELECT nextval('" + seqName + "_seq');");
            return res[0].nextval + "/" + protocol_date;
        }
        catch (error) {
            throw error;
        }
    }
}
exports.PostgreSqlEngine = PostgreSqlEngine;
_PostgreSqlEngine_table = new WeakMap(), _PostgreSqlEngine_columnNames = new WeakMap(), _PostgreSqlEngine_client = new WeakMap();
exports.default = PostgreSqlEngine;
