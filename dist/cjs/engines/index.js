"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgreSqlEngine = void 0;
var PostgreSqlEngine_js_1 = require("./postgresql/PostgreSqlEngine.js");
Object.defineProperty(exports, "PostgreSqlEngine", { enumerable: true, get: function () { return __importDefault(PostgreSqlEngine_js_1).default; } });
