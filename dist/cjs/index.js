"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostgreSqlEngine = void 0;
var index_js_1 = require("./engines/index.js");
Object.defineProperty(exports, "PostgreSqlEngine", { enumerable: true, get: function () { return index_js_1.PostgreSqlEngine; } });
