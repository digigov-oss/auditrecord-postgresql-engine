import { AuditRecord, AuditEngine, DatabaseSettings, PNRESETTYPES } from '@digigov-oss/gsis-audit-record-db';
/**
 * @description AuditEngine implementation
 * @note This class is used to implement the methods that must be implemented by the AuditEngine
 * @class PostgreSqlEngine
 * @implements AuditEngine
 * @param {string} connectionString - connect to db via connection string
 * @param {DatabaseSettings} dbSettings - database settings
 */
export declare class PostgreSqlEngine implements AuditEngine {
    #private;
    /**
     * @description constructor
     * @param {string} connectionString - connect to db via connection string
     * @param {DatabaseSettings} settings - settings for the database
     * @param {AuditRecord} columnNames - column names
     * @memberof PostgreSqlEngine
     */
    constructor(connectionString?: string, dbSettings?: DatabaseSettings);
    /**
     * @description Store a record in the database
     * @param {AuditRecord} record - record to be stored
     * @returns {AuditRecord} - the record stored
     * @memberof FileEngine
     * @method put
     */
    put(record: AuditRecord): AuditRecord;
    /**
     * @description Get a record from the database
     * @param auditTransactionId: string - transaction id
     * @returns {AuditRecord}
     * @memberof FileEngine
     * @method get
     */
    get(auditTransactionId: string): AuditRecord;
    /**
     * @description Generate a new sequence number
     * @param path
     * @returns number
     * @memberof FileEngine
     * @method seq
     */
    seq(): number;
    /**
     * @description Generate a new protocol number
     * @param path
     * @returns string
     * @memberof FileEngine
     * @method protocol
     */
    pn(pnreset?: PNRESETTYPES): string;
}
export default PostgreSqlEngine;
